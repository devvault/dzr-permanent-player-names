	/// ALL THIS CODE GOES AFTER "class CustomMission: MissionServer {" in your init.c
	/// ВСТАВЬТЕ ЭТОТ КОД ПОСЛЕ class "CustomMission: MissionServer {" в вашем init.c

	////////////////////////////////////// DZR Permanent Player Names v0.3

	/// \\\\\\\\\\\\\\\\\\ RU ///////////////////
	/// Читает playersWhiteNames.txt из папки profiles\DZR.
	/// Сравнивает имя подключающегося игрока с именем из файла по GUID 
	/// Если совпало, приветствует и пускает.
	/// Если у игрока другое имя, пишет ему об этом в чат и кикает через 45 секунд.
	/// Формат файла:
	/// Steam UID,Нужное имя игрока
	/// 	Пример:
	/// 	76562298156537007,Mikhail
	/// 	76561998116927207,Znahar
	/// Перед номером нет пробелов. До и после запятой нет пробелов. В конце строки нет пробелов и запятых.
	/// Из-за бага в дивжке нельзя использовать кириллицу.

	/// \\\\\\\\\\\\\\\\\\ EN ///////////////////
	/// Reads playersWhiteNames.txt from profiles\DZR.
	/// Matches the connecting player name with player name by guid from the file.
	/// If matches, greets and allows to the server.
	/// If name is changed or user is not on the list, informs about it in chat and kicks after 45 seconds.
	/// File format:
	/// Steam UID,Required Player Name
	/// 	Пример:
	/// 	76562298156537007,Mikhail
	/// 	76561998116927207,Znahar
	/// No space before number, no space after number, no space and comma after name.
	/// Due to a bug in the Engine, cyrillic nmes not supported.

	// TODO: 
	// - Clean up unused and irrelevant code.
	// - Make a mod instead of init.c

	void DZR_InformAllPlayers(string message)
	{
		GetGame().ChatPlayer(message);
		//Print("::: init.c ::: DZR_InformAllPlayers(string message) ::: message: " + message);
	}

	//Messaging all players
	//USAGE: DZR_MessageAllPlayers("TEXT");
	void DZR_MessageAllPlayers(string message) 
	{ 
		ref array<Man> players = new array<Man>; 
		GetGame().GetPlayers( players );
		if ( players.Count() > 0 )
		{
			for ( int i = 0; i < players.Count(); i++ ) 
			{ 
				PlayerBase player; 
				Class.CastTo(player, players.Get(i)); 
				Param1<string> m_MessageParam = new Param1<string>(message); 
				GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, m_MessageParam, true, player.GetIdentity()); 
			} 
			//Print("::: init.c ::: DZR_MessageAllPlayers(string message) ::: m_MessageParam: " + message);
		}
	}
	void DZR_SendPersonalMessage(string message, Man player) 
	{
		if(( player ) && (message != ""))
		{
			Param1<string> m_GlobalMessage = new Param1<string>(message); 
			GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, m_GlobalMessage, true, player.GetIdentity()); 
		}
	}
	void DZR_SendGlobalChatMessage(string message)
	{
		private array<Man> players = new array<Man>;
		GetGame().GetPlayers( players );
		int numbOfplayers = players.Count();
		
		if( numbOfplayers > 0 )
		{
			foreach(Man player: players)
			{
				DZR_SendPersonalMessage(message, player);
			}
		}
	}

	override void OnClientReadyEvent(PlayerIdentity identity, PlayerBase player)
	{
		Print("::: init.c ::: DZR Permanent Player Names ::: Initiated");
		super.OnClientReadyEvent(identity, player);
		//READ FILE
		ref map<string, string> m_playersSetUID = new map<string, string>;
		array<string> strFileParam;
		
		string line_content; 
		FileHandle file = OpenFile("$profile:DZR/playersWhiteNames.txt", FileMode.READ);
		Print("::: OpenFile : playersWhiteNames.txt : $profile");
		
		if (file != 0)
		{ 
			while ( FGets( file,  line_content ) > 0 )    
			{
				strFileParam = new array<string>;
				line_content.Split( ",", strFileParam );
				//Print("::: Split : strFileParam.Get(0)" + strFileParam.Get(0));
				//Print("::: Split : strFileParam.Get(1)" +strFileParam.Get(1));
				m_playersSetUID.Insert(strFileParam.Get(0), strFileParam.Get(1));
			}
			CloseFile(file);
		}
		//READ FILE END
		
		GetGame().SelectPlayer(identity, player);
		PlayerIdentity p_identity = player.GetIdentity();
		string utf_name = p_identity.GetName();
		//Print("::: init.c ::: OnClientReadyEvent utf_name ::: " + utf_name);
		
		ref array<Man> players = new array<Man>;
		GetGame().GetPlayers( players );
		
		int p_id = p_identity.GetPlayerId();
		string whiteName;
		if( p_identity )
		{
			string uid_pl = p_identity.GetPlainId();
			//Print("::: init.c ::: uid_pl ::: " + uid_pl);
			if (m_playersSetUID.Contains(uid_pl))
			{
				whiteName = m_playersSetUID.Get(uid_pl);
				//Print("::: init.c ::: whiteName ::: " + whiteName);
			} 
		}
		Print("::: init.c ::: Starting name validation::: "+whiteName+" ? "+utf_name);
		if(whiteName != utf_name){
			string Message1 = "Имя игрока на форуме ("+whiteName+") не совпадает с именем персонажа " + utf_name + "! Игрок будет отключён.";
			GetGame().ChatPlayer(Message1);
			Print("::: init.c ::: NameMismatch ::: " + Message1);
			Print ("::: init.c ::: Player disconnect: Name: " + utf_name + ", UID: " + p_identity.GetPlainId()+": Name Mismatch");
			DelayedKickPlayer(player, p_identity, 1, whiteName, 45);		
			} else {
			// Message
			Print("::: init.c ::: Name "+utf_name +" validated: OK ::: ");
			Param1<string> Msgparam1;
			Msgparam1 = new Param1<string>("Игрок " + identity.GetName() + ". Имя проверено: ОК. ");
			GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(GetGame().RPCSingleParam, (7 * 100), false, player, ERPCs.RPC_USER_ACTION_MESSAGE, Msgparam1, true, identity );
		};
		//GUID PROCESS END
	};

	void KickPlayer(PlayerBase player, PlayerIdentity identity, int m_ListType)
	{
		if (player.GetIdentity())
		{
			if (!identity) identity	=	player.GetIdentity();
			string PlayerUID		=	identity.GetPlainId();
			string PlayerNAME		=	identity.GetName();
			
			if (player)
			{
				GetGame().SendLogoutTime(player, 15);
				
				if (GetHive())
				{
					// save player
					player.Save();
					// unlock player in DB	
					GetHive().CharacterExit(player);		
				}
				
				player.ReleaseNetworkControls();
				if (player.IsAlive() && !player.IsRestrained() && !player.IsUnconscious())
				{
					// remove the body
					player.Delete();	
				}
				else if (player.IsUnconscious() || player.IsRestrained())
				{
					// kill character
					player.SetHealth("", "", 0.0);
				}
			}
			// remove player from server
			GetGame().DisconnectPlayer(identity);
			Print("::: init.c ::: DZR Permanent Player Names::: KickPlayer() ::: : player: " + PlayerNAME + ", UID: " + PlayerUID);
		}
		else
		{
			Print("::: init.c ::: DZR Permanent Player Names::: KickPlayer() ::: Kick error: PlayerIdentity NULL!");
		}
	}

	void DelayedKickPlayer(PlayerBase player, PlayerIdentity identity, int m_ListType, string whiteName, int i_delay)
	{
		
		Print("::: init.c ::: DZR Permanent Player Names::: DelayedKickPlayer() ::: Kick player: Name: identity.GetName(): " + identity.GetName() + ", UID: identity.GetPlainId(): " + identity.GetPlainId());
		// Message
		Param1<string> Msgparam1;
		Msgparam1 = new Param1<string>("Имя персонажа " + identity.GetName() + " не совпадает с именем на форуме: "+whiteName);
		Param1<string> Msgparam2 = new Param1<string>("Игрок будет отключен через "+i_delay+" с.");
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(GetGame().RPCSingleParam, (i_delay * 500), false, player, ERPCs.RPC_USER_ACTION_MESSAGE, Msgparam1, true, identity );
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(GetGame().RPCSingleParam, (i_delay * 550), false, player, ERPCs.RPC_USER_ACTION_MESSAGE, Msgparam2, true, identity );
		
		// Delayed kick
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(KickPlayer, (i_delay * 1500), false, player, identity, 1);
	}	
	////////////////////////////////////// DZR Permanent Player Names v0.3