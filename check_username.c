	//-------------- DZR NAME VALIDATION --------------
	//MOD

	/// ===================== НАСТРОЙКИ \ CONFIG ======================
	int pKickTime = 45; //Kick delay
	string pLanguage = "EN"; //EN = English, RU = Russian
	/// ===================== НАСТРОЙКИ \ CONFIG ======================

	const static string dzr_Init_ProfileFolder = "$profile:\\";
	const static string dzr_Init_TagFolder = "DZR\\"; // Storage prefix
	const static string dzr_Init_ModFolder = "players\\"; // player storage
	const static string dzr_Init_cOnceFolder = dzr_Init_ProfileFolder+dzr_Init_TagFolder+dzr_Init_ModFolder+"connectedOnlyOnce\\";
	const static string dzr_Init_pProfilesFolder = dzr_Init_ProfileFolder+dzr_Init_TagFolder+dzr_Init_ModFolder+"playerProfiles\\";
	const static string dzr_Init_sNamesFolder = dzr_Init_ProfileFolder+dzr_Init_TagFolder+dzr_Init_ModFolder+"savedNames\\"; // Saved names are here and editable

	void InformAllPlayers(string message)
	{
		GetGame().ChatPlayer(message);
	}

	//MZ
	//Messaging all players
	//USAGE: MessageAllPlayers("TEXT");
	void MessageAllPlayers(string message) 
	{ 
		ref array<Man> players = new array<Man>; 
		GetGame().GetPlayers( players );
		if ( players.Count() > 0 )
		{
			for ( int i = 0; i < players.Count(); i++ ) 
			{ 
				PlayerBase player; 
				Class.CastTo(player, players.Get(i)); 
				Param1<string> m_MessageParam = new Param1<string>(message); 
				GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, m_MessageParam, true, player.GetIdentity()); 
			} 
		}
	}

	void SendPersonalMessage(string message, Man player) 
	{
		if(( player ) && (message != ""))
		{
			Param1<string> m_GlobalMessage = new Param1<string>(message); 
			GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, m_GlobalMessage, true, player.GetIdentity()); 
		}
	}

	void SendGlobalChatMessage(string message)
	{
		private array<Man> players = new array<Man>;
		GetGame().GetPlayers( players );
		int numbOfplayers = players.Count();
		
		if( numbOfplayers > 0 )
		{
			foreach(Man player: players)
			{
				SendPersonalMessage(message, player);
			}
		}
	}

	void checkInitDirectories (){
		if (!FileExist(dzr_Init_ProfileFolder+dzr_Init_TagFolder)){
			MakeDirectory(dzr_Init_ProfileFolder+dzr_Init_TagFolder);	
			Print("::: [init_dzr] :: Creating folder: "+dzr_Init_ProfileFolder+dzr_Init_TagFolder);
		}
		if (!FileExist(dzr_Init_ProfileFolder+dzr_Init_TagFolder+dzr_Init_ModFolder)){
			MakeDirectory(dzr_Init_ProfileFolder+dzr_Init_TagFolder+dzr_Init_ModFolder);	
			Print("::: [init_dzr] :: Creating folder: "+dzr_Init_ProfileFolder+dzr_Init_TagFolder+dzr_Init_ModFolder);
		}
		if (!FileExist(dzr_Init_cOnceFolder)){
			MakeDirectory(dzr_Init_cOnceFolder);	
			Print("::: [init_dzr] :: Creating folder: "+dzr_Init_cOnceFolder);
		}
		if (!FileExist(dzr_Init_pProfilesFolder)){
			MakeDirectory(dzr_Init_pProfilesFolder);	
			Print("::: [init_dzr] :: Creating folder: "+dzr_Init_pProfilesFolder);
		}
		if (!FileExist(dzr_Init_sNamesFolder)){
			MakeDirectory(dzr_Init_sNamesFolder);	
			Print("::: [init_dzr] :: Creating folder: "+dzr_Init_sNamesFolder);
		}
	}

	bool isFirstConnect(string pGuid){
		//does not exist in connectedOnlyOnce
		string m_dzrInitFilePath2 = dzr_Init_cOnceFolder+"\\"+pGuid+".txt";
		if (!FileExist(m_dzrInitFilePath2)) 
		{
			return true;
		}
		return false;
	}

	bool isNameAlreadyTaken(string pName)
	{
		
		string line_content; 
		string	file_name;
		int 	file_attr;
		int		flags;
		
		
		string path_find_pattern = dzr_Init_sNamesFolder+"\\*.txt"; 
		FindFileHandle file_handler = FindFile(path_find_pattern, file_name, file_attr, flags);
		
		bool found = true;
		while ( found )
		{			
			
			found = FindNextFile(file_handler, file_name, file_attr);	
			FileHandle file = OpenFile(dzr_Init_sNamesFolder+"\\"+file_name, FileMode.READ);
			bool fileGetting = FGets( file,  line_content );
			Print("::: [init_dzr] :: isNameAlreadyTaken :: fileGetting: "+fileGetting+", line_content: "+line_content+", pName: "+pName);
			CloseFile(file);
			if(line_content == pName) 
			{
				return true;
			}
		}
		
		return false;
	}

	bool isNameSaved(string pGuid){
		//esxists in SavedNames
		//string m_dzrInitFilePath1 = dzr_Init_ProfileFolder+dzr_Init_TagFolder+dzr_Init_ModFolder+dzr_Init_sNamesFolder;
		string m_dzrInitFilePath2 = dzr_Init_sNamesFolder+"\\"+pGuid+".txt";
		if (FileExist(m_dzrInitFilePath2)) 
		{
			return true;
		}
		return false;
	}

	string isNameCorrect(string pGuid, string pName)
	{
		string line_content; 
		if( isNameSaved(pGuid) )
		{
			//if exists isNameSaved
			FileHandle file = OpenFile(dzr_Init_sNamesFolder+"\\"+pGuid+".txt", FileMode.READ);
			FGets( file,  line_content );
			CloseFile(file);
			if( line_content == pName || line_content == "0") 
			{
				return "1";
				// if pName = pGuid
				// 1
			}
			else 
			{
				// name mismatch
				// 0
				return line_content;
			}
		}
		else 
		{
			//not saved
			// 2
			return "2";
		}
	}

	bool SaveName (string pGuid, string pName)
	{
		//delete 
		
		string fConnected = dzr_Init_sNamesFolder+"\\"+pGuid+".txt";
		//string fNeverConnected = "$profile:DZR/players/never_connected";
		if ( !FileExist(fConnected) )
		{
			FileHandle fhandle	=	OpenFile(fConnected, FileMode.WRITE);
			FPrintln(fhandle, pName);
			//not found in connected, create the file with player
			CloseFile(fhandle);
			return true;
		} 
		else 
		{
			return false;
		}
		
	}

	bool okConnectedFirstTime(string pGuid, string pName)
	{
		string fConnected = dzr_Init_cOnceFolder+"\\"+pGuid+".txt";
		//string fNeverConnected = "$profile:DZR/players/never_connected";
		if ( !FileExist(fConnected) )
		{
			FileHandle fhandle	=	OpenFile(fConnected, FileMode.WRITE);
			FPrintln(fhandle, pName);
			//not found in connected, create the file with player
			CloseFile(fhandle);
			return true;
		} 
		else
		{
			return false;
		}
	}

	void NewNotification(string Message1, string MessageTitle, string old_icon, int Old_color, int timer, PlayerIdentity identity)
	{
		NotificationSystem.SendNotificationToPlayerIdentityExtended( identity , timer, MessageTitle, Message1, "set:ccgui_enforce image:MapShieldBooster");
	};

	void ProcessNameSaving(PlayerBase player, PlayerIdentity identity, string pGuid, string pName)
	{
		if( isFirstConnect(pGuid) )
		{
			//if isFirstConnect
			// set isFirstConnect 1
			okConnectedFirstTime(pGuid, pName);
			//if isNameAlreadyTaken
			if( isNameAlreadyTaken(pName) )
			{
				if(pLanguage == "EN"){
					NewNotification("Change your name in DayZ Launcher - Parameters - Name. Next time you connect, your new name will be saved if not taken too.", "Name "+pName+" is taken", "Notification/gui/data/notifications.edds", ARGB(240, 255, 255, 0), 15, identity);
				}
				if(pLanguage == "RU"){
					NewNotification("Измените имя в лончере DayZ - Параметры - Имя профиля. В следующий раз имя сохранится, если тоже не занято.", "Имя "+pName+" занято", "Notification/gui/data/notifications.edds", ARGB(240, 255, 255, 0), 15, identity);
				}
				Print("::: [init_dzr] :: Name "+pName+" is taken");
				//Title, NAME1 is taken, use another
				// say, next time the your character name NAME1 will be saved forever.
				// say, you will be kicked if with any other name than NAME1
				//NEVER KICK
			}
			//else
			else 
			{
				if(pLanguage == "EN"){
					NewNotification("Next time you connect, your name ("+pName+") will be saved forever on this server for this Steam account. You will be kicked with any other name in the future. You have a chance to change name in launcher - Parameters - Profile Name. This message will remain for 3 minutes.", "Permanent name!", "Notification/gui/data/notifications.edds", ARGB(240, 255, 123, 0), 300, identity);
				}
				if(pLanguage == "RU"){
					NewNotification("В следующий раз имя "+pName+" навсегда сохранится на сервере для данного аккаунта Steam. После этого с любым другим именем вас будет кикать с сервера. У вас есть шанс сейчас сменить имя в лончере - Параметры - Имя. Это сообщение исчезнет через 3 минуты.", "Имя сохранится навсегда!", "Notification/gui/data/notifications.edds", ARGB(240, 255, 123, 0), 300, identity);
				}
				Print("::: [init_dzr] :: "+pName+" will be saved permanently as the name for player "+pGuid);
			}
		}
		else
		{
			//if !isFirstConnect
			// switch isNameCorrect
			// isNameCorrect 0
			// Title, Incorrect Character Name: Kick in 25 s.
			// Your name: NAME1
			// Server saved name: NAME2
			// Change name in Launcher Parameters
			string nameValidation = isNameCorrect(pGuid, pName);
			if( nameValidation != "1" && nameValidation != "2")
			{
				DelayedKickPlayer(player, identity, pKickTime);
				if(pLanguage == "EN"){
					NewNotification(pName+" is not matching your saved server name for this Steam account. Change it to "+nameValidation+" in Launcher - Parameters - Name. Kick in "+pKickTime.ToString()+" s.", "Wrong Name: "+nameValidation, "Notification/gui/data/notifications.edds", ARGB(240, 255, 0, 0), pKickTime, identity);
				}
				if(pLanguage == "RU"){
					NewNotification(pName+" не совпадает с именем, сохранённым для вашего Steam аккаунта. Смените гео на "+nameValidation+" в лончере - Параметры - Имя профиля. Kick in "+pKickTime+" s.", "Wrong Name: "+nameValidation, "Notification/gui/data/notifications.edds", ARGB(240, 255, 0, 0), pKickTime, identity);
				}
				Print("::: [init_dzr] :: "+pName+" not matching "+nameValidation+" for player "+pGuid);
			}
			
			switch( nameValidation )
			{
				case "1":
				if(pLanguage == "EN"){
					NewNotification("The name "+pName+" is verified. Welcome.", "Character Name", "Notification/gui/data/notifications.edds", ARGB(240, 0, 176, 44), 15, identity);
				}
				if(pLanguage == "RU"){
					NewNotification("Имя ("+pName+") подтверждено. Добро пожаловать.", "Имя персонажа", "Notification/gui/data/notifications.edds", ARGB(240, 0, 176, 44), 15, identity);
				}
				// isNameCorrect 1
				//Name checked: OK
				//Your name: NAME1
				
				break;
				case "2":

				// isNameCorrect 2
				// if isNameAlreadyTaken
				if( isNameAlreadyTaken(pName) )
				{
					DelayedKickPlayer(player, identity, pKickTime);
					if(pLanguage == "EN"){
						NewNotification("Change your name in DayZ Launcher - Parameters - Name. Next time you connect, your new name will be saved if not taken too. Kick in "+pKickTime+" s.", "Name "+pName+" is taken", "Notification/gui/data/notifications.edds", ARGB(240, 255, 255, 0), pKickTime, identity);
					}
					if(pLanguage == "RU"){
						NewNotification("Измените имя в лончере DayZ - Параметры - Имя профиля. В следующий раз имя сохранится, если тоже не занято. Вас кикнет с сервера через "+pKickTime+" с.", "Имя "+pName+" занято", "Notification/gui/data/notifications.edds", ARGB(240, 255, 255, 0), pKickTime, identity);
					}
					Print("::: [init_dzr] :: "+pName+" taken. Player "+pGuid+" will be kicked");					
					
					//Title, NAME1 is taken, use another
					// say, next time the your character name NAME1 will be saved forever.
					// say, you will be kicked if with any other name than NAME1
					//NEVER KICK
				}
				//  not taken
				// all fine, SAVE IT!
				// savePlayerProfile set DateJoined
				else 
				{
					if( SaveName(pGuid, pName) )
					{
						nameValidation = isNameCorrect(pGuid, pName);
						// if isNameCorrect 1
						// Title, Character name NAME1 now saved for this Steam account.
						// Use only NAME1 on this server, or you will get kicked.
						// Other players will not be able to take your name.
						// Welcome!
						if( nameValidation == "1" ){
							if(pLanguage == "EN"){
								NewNotification(pName+" — is now your permanent name on this server. Using any other name will kick you from server.", "Your name is now permanent", "Notification/gui/data/notifications.edds", ARGB(240, 0, 176, 44), 15, identity);
							}
							if(pLanguage == "RU"){
								NewNotification(pName+" — это теперь ваше имя на сервере. Сервер кикнет вас с любым другим именем.", "Имя сохранено навсегда", "Notification/gui/data/notifications.edds", ARGB(240, 0, 176, 44), 15, identity);
							}
							Print("::: [init_dzr] :: The name "+pName+" is now permanent for player "+pGuid);
							} else {
							DelayedKickPlayer(player, identity, pKickTime);
							if(pLanguage == "EN"){
								NewNotification(pName+" COULD NOT BE SAVED. CONTACT ADMIN OR USE OTHER NAME.", "ERROR SAVING YOUR NAME. KICK IN "+pKickTime+" s.", "Notification/gui/data/notifications.edds", ARGB(240, 255, 0, 0), pKickTime, identity);
							}
							if(pLanguage == "RU"){
								NewNotification(pName+" НЕ СОХРАНЯЕТСЯ! СООБЩИТЕ АДМИНУ ИЛИ ПОПРОБУЙТЕ ДРУГОЕ ИМЯ. ОТКЛЮЧЕНИЕ ЧЕРЕЗ "+pKickTime+" c. ", "НЕИЗВЕСТНАЯ ОШИБКА", "Notification/gui/data/notifications.edds", ARGB(240, 255, 0, 0), pKickTime, identity);
							}
							Print("::: [init_dzr] :: UNKNOWN ERROR!!! Saving the name "+pName+" failed for player "+pGuid);
						}
					}
				}
				break;
			}
			
		}
	}

	override void OnClientReadyEvent(PlayerIdentity identity, PlayerBase player)
	{
		checkInitDirectories();
		GetGame().SelectPlayer(identity, player);
		PlayerIdentity p_identity = player.GetIdentity();
		
		string utf_name = p_identity.GetName();

		string numGuid = p_identity.GetPlainId();
		
		Print("::: [init_dzr] ::  Starting name validation: "+utf_name);
		ProcessNameSaving(player, p_identity, numGuid, utf_name);
		//////////////////////////////
		
		//GUID PROCESS END
		
	};

	void KickPlayer(PlayerBase player, PlayerIdentity identity, int m_ListType)
	{
		if (player.GetIdentity())
		{
			if (!identity) identity	=	player.GetIdentity();
			string PlayerUID		=	identity.GetPlainId();
			string PlayerNAME		=	identity.GetName();

			if (player)
			{
				GetGame().SendLogoutTime(player, 15);
				
				if (GetHive())
				{
					// save player
					player.Save();
					// unlock player in DB	
					GetHive().CharacterExit(player);		
				}
				
				player.ReleaseNetworkControls();
				
				if (player.IsAlive() && !player.IsRestrained() && !player.IsUnconscious())
				{
					// remove the body
					player.Delete();	
				}
				else if (player.IsUnconscious() || player.IsRestrained())
				{
					// kill character
					player.SetHealth("", "", 0.0);
				}
			}
			
			// remove player from server
			GetGame().DisconnectPlayer(identity);
			
			//Print("::: [init_dzr] ::  KickPlayer() ::: : player: " + PlayerNAME + ", UID: " + PlayerUID);
		}
		else
		{
			Print("::: [init_dzr] ::  KickPlayer() ::: Kick error: PlayerIdentity NULL!");
		}
	}

	void DelayedKickPlayer(PlayerBase player, PlayerIdentity identity, int i_delay)
	{
		
		//Print("::: [init_dzr] ::  DelayedKickPlayer("+i_delay*1000+") ::: Kick player: Name: identity.GetName(): " + identity.GetName() + ", UID: identity.GetPlainId(): " + identity.GetPlainId());
		Print("::: [init_dzr] :: Kicking player " + identity.GetName() + " ("+identity.GetPlainId()+") in "+i_delay*1000+" s.");
		
		// Delayed kick
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(KickPlayer, (i_delay*1000+5), false, player, identity, 1);
	}	

	//MOD end
	//-------------- DZR NAME VALIDATION END --------------
